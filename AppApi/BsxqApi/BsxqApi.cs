﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tools;
using Tools.Http;
using Newtonsoft.Json;
using AppApi.BsxqApi.Output;
using Public;

namespace AppApi.BsxqApi
{
    public class BsxqApi
    {
        public void GetBlue()
        {


        }

        /// <summary>
        /// 挖矿
        /// </summary>
        public BaseResult BatchDiggle(BsxqInput input)
        {
            BaseResult result = new BaseResult();
            try
            {
                
                HttpInput Hinput = new HttpInput();
                Hinput.Url = "http://interface.baoshixingqiu.com/diggle/batch-diggle?device=android&versionCode=114&token=" + input.Token;
                Hinput.AcceptLanguage = "zh-CN,zh;q=0.8";
                Hinput.UserAgent = "Mozilla/5.0 (Linux; U; Android 8.0.0; zh-cn; MIX 2S Build/OPR1.170623.032) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
                Hinput.Host = "interface.baoshixingqiu.com";
                Hinput.AcceptEncoding = "gzip";
                var value = HttpHelp.HttpPost(Hinput, true);
                var Jsonvalue = JsonConvert.DeserializeObject<BsxqBaseOutput<BsxqBatchDiggleData>>(value);

                if (Jsonvalue.code == 0)
                    result.IsSuccess = true; 

                else
                    result.IsSuccess = false;
                result.Remake = Jsonvalue.msg;
                return result;


            }
            catch (Exception ex)
            {
                result.IsSuccess = false;result.Remake = "错误:" + ex.ToString();
                return result;
            }
            
        }
        /// <summary>
        /// 签到
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public BaseResult reward(BsxqInput input)
        {
            BaseResult result = new BaseResult();
            try
            {

                HttpInput Hinput = new HttpInput();
                Hinput.Url = "http://interface.baoshixingqiu.com/act/index/reward?device=android&versionCode=114&token="+input.Token+"&id=176&task_version=ccd05fe8d7ca756b1abe04338940e54d ";
                Hinput.AcceptLanguage = "zh-CN,zh;q=0.8";
                Hinput.UserAgent = "Mozilla/5.0 (Linux; U; Android 8.0.0; zh-cn; MIX 2S Build/OPR1.170623.032) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
                Hinput.Host = "interface.baoshixingqiu.com";
                Hinput.AcceptEncoding = "gzip";
                var value = HttpHelp.HttpPost(Hinput, true);
                var Jsonvalue = JsonConvert.DeserializeObject<BsxqBaseOutput<BsxqBaseOutput>>(value);

                if (Jsonvalue.code == 0)
                    result.IsSuccess = true;

                else
                    result.IsSuccess = false;
                result.Remake = Jsonvalue.msg;
                return result;


            }
            catch (Exception ex)
            {
                result.IsSuccess = false; result.Remake = "错误:" + ex.ToString();
                return result;
            }

        }
    }
}
