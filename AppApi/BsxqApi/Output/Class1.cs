﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppApi.BsxqApi.Output
{

    public class BsxqBatchDiggleData
    {
        public BsxqBatchDiggleInfo info { get; set; }

        public List<BsxqBatchDiggleWindowItem> window { get; set; }
    }

    public class BsxqBatchDiggleInfo
    {
        public string red { get; set; }

        public string blue { get; set; }
    }

    //public class BsxqBatchDiggleWindow
    //{
      
    //}

    public class BsxqBatchDiggleWindowItem
    {
        public string pic { get; set; }

        public string number { get; set; }
    }
}
