﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppApi.BsxqApi.Output
{
    public class BsxqTasksOutput
    {
        public string code { get; set; }

        public string msg { get; set; }

        
    }
    public class BsxqTasksOutput<T>
    {
        public string code { get; set; }

        public T data { get; set; }

        public string msg { get; set; }
    }

    public class BsxqTasksListOutput
    {

    }


}
