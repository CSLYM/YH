﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppApi.BsxqApi.Output
{
    public class BsxqBaseOutput
    {
        public int code { get; set; }

        public string msg { get; set; }
    }
    public class BsxqBaseOutput<T>
    {
        public int code { get; set; }
        
        public T data { get; set; }

        public string msg { get; set; }
    }
}
