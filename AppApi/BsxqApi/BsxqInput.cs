﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppApi.BsxqApi
{
    public class BsxqInput
    {
        public string Device { get; set; }

        public string VersionCode { get; set; }

        public string Token { get; set; }
    }
}
