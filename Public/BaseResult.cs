﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Public
{
    public class BaseResult
    {
        public bool IsSuccess { get; set; }

        public string Code { get; set; }

        public string Remake { get; set; }
    }
}
