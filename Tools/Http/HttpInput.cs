﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tools.Http
{
    public class HttpInput
    {
        public string Url { get; set; }
        public string ContentType { get; set; }

        public string PostData { get; set; }

        public string Cookie { get; set; }

        public string UserAgent { get; set; }

        public string AcceptEncoding { get; set; }

        public string AcceptLanguage { get; set; }

        public string Host { get; set; }

    }
}
