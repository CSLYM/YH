﻿namespace FrmLp
{
    partial class Yh_Bsxq
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Dgv_Text = new System.Windows.Forms.DataGridView();
            this.user_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.token = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Chek_Sign = new System.Windows.Forms.CheckBox();
            this.Chek_BatchDiggle = new System.Windows.Forms.CheckBox();
            this.Btn_Run = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_Text)).BeginInit();
            this.SuspendLayout();
            // 
            // Dgv_Text
            // 
            this.Dgv_Text.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv_Text.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.user_id,
            this.token});
            this.Dgv_Text.Location = new System.Drawing.Point(12, 12);
            this.Dgv_Text.Name = "Dgv_Text";
            this.Dgv_Text.RowTemplate.Height = 23;
            this.Dgv_Text.Size = new System.Drawing.Size(414, 426);
            this.Dgv_Text.TabIndex = 0;
            // 
            // user_id
            // 
            this.user_id.HeaderText = "帐号";
            this.user_id.Name = "user_id";
            // 
            // token
            // 
            this.token.HeaderText = "token";
            this.token.Name = "token";
            // 
            // Chek_Sign
            // 
            this.Chek_Sign.AutoSize = true;
            this.Chek_Sign.Location = new System.Drawing.Point(449, 27);
            this.Chek_Sign.Name = "Chek_Sign";
            this.Chek_Sign.Size = new System.Drawing.Size(48, 16);
            this.Chek_Sign.TabIndex = 1;
            this.Chek_Sign.Text = "签到";
            this.Chek_Sign.UseVisualStyleBackColor = true;
            // 
            // Chek_BatchDiggle
            // 
            this.Chek_BatchDiggle.AutoSize = true;
            this.Chek_BatchDiggle.Location = new System.Drawing.Point(547, 27);
            this.Chek_BatchDiggle.Name = "Chek_BatchDiggle";
            this.Chek_BatchDiggle.Size = new System.Drawing.Size(48, 16);
            this.Chek_BatchDiggle.TabIndex = 2;
            this.Chek_BatchDiggle.Text = "挖矿";
            this.Chek_BatchDiggle.UseVisualStyleBackColor = true;
            // 
            // Btn_Run
            // 
            this.Btn_Run.Location = new System.Drawing.Point(449, 143);
            this.Btn_Run.Name = "Btn_Run";
            this.Btn_Run.Size = new System.Drawing.Size(75, 23);
            this.Btn_Run.TabIndex = 3;
            this.Btn_Run.Text = "开始";
            this.Btn_Run.UseVisualStyleBackColor = true;
            this.Btn_Run.Click += new System.EventHandler(this.Btn_Run_Click);
            // 
            // Yh_Bsxq
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Btn_Run);
            this.Controls.Add(this.Chek_BatchDiggle);
            this.Controls.Add(this.Chek_Sign);
            this.Controls.Add(this.Dgv_Text);
            this.Name = "Yh_Bsxq";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_Text)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Dgv_Text;
        private System.Windows.Forms.DataGridViewTextBoxColumn user_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn token;
        private System.Windows.Forms.CheckBox Chek_Sign;
        private System.Windows.Forms.CheckBox Chek_BatchDiggle;
        private System.Windows.Forms.Button Btn_Run;
    }
}

